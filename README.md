## About
A simple C program that outputs a list of ANSI-escape-code colors and 24-bit color hex values based on a given input PPM image user k-means clustering.

## Building
```bash
./compile.sh
```

## Usage
```bash
./finder <number_of_colors> <max_kmeans_iterations> <input_image>.ppm 
```

### Example usage
```bash
./finder 4 1000 cat.ppm
```

Output:
```
#98967d
#232421
#bdc3c2
#615f4e
```

## Other
`max_kmeans_iterations` is the max limit of computation rounds to be used for the k-means algorithm to converge.
The program will automatically detect if the points have converged and prematurely exit the loop if no more computation is needed.

PPM input image cannot have comments.

## License
MIT
