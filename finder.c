// vim:fileencoding=utf-8:foldmethod=marker
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

/*
	Argument order:
		0:	Program
		1:	Number of colors to extract.
		2:	Iterations.
		3:	PPM image file.
*/

// Pixel color point struct. {{{
struct colorPoint
{
	float r;
	float g;
	float b;
	int closestTo;
};

// }}}

// Voronoi point struct. {{{
struct voronoiPoint
{
	float r;
	float g;
	float b;
};

// }}}

// 3D distance function. {{{
float dist(float x1, float z1, float y1, float x2, float z2, float y2)
{
	return sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)) + ((z2 - z1) * (z2 - z1)));
}

// }}}

int main(int argc, char * argv[])
{
	// Checking for correct number of arguments. {{{
	if(argc != 4)
	{
		printf("Usage:\n\tfinder <number_of_colors> <iterations> <input_ppm_file>\n\n");

		return 1;
	}

	// }}}

	// Getting number of iterations to run program for.
	int iterations = atoi(argv[2]);

	// Getting the number of colors to extract.
	int nColors = atoi(argv[1]);

	// Reading image info into memory. {{{
	// Declaring the width and height variables for the image.
	int width, height = 0;

	// Opening image file.
	FILE * fp = fopen(argv[3], "r");

	// Checking for errors opening input file.
	if(fp == NULL)
	{
		printf("Could not open input file \"%s\"\n", argv[3]);

		return 1;
	}

	// Reading in image file specs.
	if(fscanf(fp, "P6\n%d %d\n255\n", &width, &height) != 2)
	{
		// Closing input file.
		fclose(fp);

		printf("Input file \"%s\" is not correctly formatted or is not a PPM image!\n", argv[3]);

		return 1;
	}

	// Declaring the pixel data buffer.
	char * buffer = (char *)malloc(sizeof(char) * width * height * 3);

	// Reading in image file data.
	fread(buffer, sizeof(char), width * height * 3, fp);

	// Closing input file.
	fclose(fp);

	// }}}

	// Declaring color point array.
	struct colorPoint colorPoints[width * height];

	// Putting pixel color data into color points. {{{
	for(int i = 0; i < width * height * 3; i += 3)
	{
		colorPoints[i / 3].r = (float)buffer[i];
		colorPoints[i / 3].g = (float)buffer[i + 1];
		colorPoints[i / 3].b = (float)buffer[i + 2];
	}

	// }}}

	// Seeding the RNG.
	srand(time(NULL));

	// Declaring voronoi point array.
	struct voronoiPoint voronoiPoints[nColors];

	// Placing voronoi points randomly. {{{
	for(int i = 0; i < nColors; i++)
	{
		voronoiPoints[i].r = (float)rand()/(float)(RAND_MAX/255);
		voronoiPoints[i].g = (float)rand()/(float)(RAND_MAX/255);
		voronoiPoints[i].b = (float)rand()/(float)(RAND_MAX/255);
	}

	// }}}

	// Main loop. {{{
	for(int i = 0; i < iterations; i++)
	{
		// For remembering if any point moved.
		int moved = 0;

		// Assigning every color point to its closest voronoi point. {{{
		for(int j = 0; j < width * height; j++)
		{
			// Declaring and setting the initial distance to something impossibly large.
			float currentSmallestDistance = 10000.0;

			// Looping over each voronoi point and recording the closest.
			for(int k = 0; k < nColors; k++)
			{
				// Getting the distance to the current voronoi point.
				float distanceToPoint = dist(
					voronoiPoints[k].r,
					voronoiPoints[k].g,
					voronoiPoints[k].b,
					colorPoints[j].r,
					colorPoints[j].g,
					colorPoints[j].b
				);

				// Recording the distance if it's smaller.
				if(distanceToPoint < currentSmallestDistance)
				{
					currentSmallestDistance = distanceToPoint;
					colorPoints[j].closestTo = k;
				}
			}
		}

		// }}}

		// Finding the average location of each color point in the voronoi point's domain. {{{
		// Looping over each voronoi point.
		for(int j = 0; j < nColors; j++)
		{
			// Declaring the average locations of each color.
			float averageR = 0.0;
			float averageG = 0.0;
			float averageB = 0.0;

			// Declaring the variables for holding the number of points in each voronoi cell.
			int points = 1;

			// Looping over each color point and adding its positions to the average if it belongs in the domain of the current voronoi point.
			for(int k = 0; k < width * height; k++)
			{
				if(colorPoints[k].closestTo == j)
				{
					averageR += colorPoints[k].r;
					averageG += colorPoints[k].g;
					averageB += colorPoints[k].b;
					points++;
				}
			}

			// Recording the locations of the point before it's updated.
			float beforeR = voronoiPoints[j].r;
			float beforeG = voronoiPoints[j].g;
			float beforeB = voronoiPoints[j].b;

			// Updating the location of the current voronoi point.
			voronoiPoints[j].r = averageR / points;
			voronoiPoints[j].g = averageG / points;
			voronoiPoints[j].b = averageB / points;

			// Checking if the point moved.
			if(beforeR != voronoiPoints[j].r || beforeG != voronoiPoints[j].g || beforeB != voronoiPoints[j].b)
			{
				moved = 1;
			}
		}

		// Breaking out of the loop if no points moved.
		if(moved == 0)
		{
			goto printResults;
		}

		// }}}
	}

	// }}}

	printResults:

	// Printing the final color locations of the voronoi points. {{{
	for(int i = 0; i < nColors; i++)
	{
		printf("\033[48;2;%d;%d;%dm      \033[0m #%02x%02x%02x\n",
		(int)voronoiPoints[i].r,
		(int)voronoiPoints[i].g,
		(int)voronoiPoints[i].b,
		(int)voronoiPoints[i].r,
		(int)voronoiPoints[i].g,
		(int)voronoiPoints[i].b);
	}

	// }}}

	// Freeing the pixel data buffer.
	free(buffer);

	return 0;
}
